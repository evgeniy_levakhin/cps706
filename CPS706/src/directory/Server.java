package directory;

import util.NodeData;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.*;

public class Server {

    private static DatagramSocket udpSocket = null;
    private static ServerSocket tcpSocket = null;
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static NodeData myData = new NodeData();
    private static Directory directory = new Directory();
    private static Map<Integer, NodeData> successors = Collections.synchronizedMap(new HashMap());
    private static boolean bRunning = true;
    private static ClientListener clientListener = null;
    private static ServerListener serverListener = null;

    public static void main(String[] args) {
        
        //String[] args = {"1", "40150"};
        
        if (args.length != 2) {
            System.err.println("Incorrect starting parameters");
            System.err.println("java APPlication ServerID PORT");
            System.exit(-1);
        }
        myData.setID(Integer.parseInt(args[0]));
        myData.setPort(Integer.parseInt(args[1]));
        
        try {
            myData.setIP(InetAddress.getLocalHost());

            udpSocket = new DatagramSocket(myData.getPort());
            tcpSocket = new ServerSocket(myData.getPort());

            clientListener = new ClientListener();
            clientListener.start();

            serverListener = new ServerListener();
            serverListener.start();

            System.out.println("Server is starting up");
            System.out.println("Listening to user input");

            while (bRunning) {
                System.out.println("To add a successor server type add");
                System.out.println("To quit type quite");

                String line = br.readLine();

                switch (line) {
                    case "add":
                        System.out.println("Please enter server ID:");
                        String id = br.readLine();
                        System.out.println("Please enter proper IPV4 address (no error checking is done):");
                        String ip = br.readLine();
                        System.out.println("Please specify the port:");
                        String port = br.readLine();
                        NodeData serverData = new NodeData();
                        serverData.setID(Integer.parseInt(id));
                        serverData.setIP(ip);
                        serverData.setPort(Integer.parseInt(port));
                        successors.put(Integer.parseInt(id), serverData);
                        System.out.println("The successor server has been added succesfully");
                        System.out.println(id + " " + ip + ":" + port);
                        break;
                    case "quit":
                        bRunning = false;
                        System.out.println("Quiting...");
                        if (!successors.isEmpty()) {
                            System.out.println("Notifying successors...");
                            for (Map.Entry<Integer, NodeData> entry : successors.entrySet()) {
                                //notify other servers about my quit.
                                NodeData data = entry.getValue();
                                System.out.println("Notifying " + data.getIP() + ":" + data.getPort());
                                try (Socket tmpSocket = new Socket(data.getIP(), data.getPort())) {
                                    ObjectOutputStream outbound = new ObjectOutputStream(tmpSocket.getOutputStream());
                                    Message msg = new Message(Message.QUIT);
                                    msg.setLoad(myData);
                                    outbound.writeObject(msg);
                                }
                            }
                        }
                        break;
                    default:
                        System.out.println("Unrecognized command " + line);
                        break;

                }
            }
            System.exit(0);
        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static class ServerListener extends Thread {

        public ServerListener() {
        }

        @Override
        public void run() {
            while (bRunning) {
                try {
                    Socket inSocket = tcpSocket.accept();
                    ObjectInputStream incoming = new ObjectInputStream(inSocket.getInputStream());
                    Message msg = (Message) incoming.readObject();

                    switch (msg.getType()) {
                        case Message.QUIT:
                            InetAddress address = ((NodeData) msg.getLoad()).getIP();
                            System.out.println("Quit message received from " + address);
                            for (Map.Entry<Integer, NodeData> entry : successors.entrySet()) {
                                //notify other servers about my quit.
                                NodeData data = entry.getValue();

                                if (data.getIP() == address) {
                                    successors.remove(entry.getKey());
                                } else {
                                    try (Socket tmpSocket = new Socket(data.getIP(), data.getPort())) {
                                        ObjectOutputStream outbound = new ObjectOutputStream(tmpSocket.getOutputStream());
                                        outbound.writeObject(msg);
                                    }
                                }
                            }
                            break;
                        case Message.REMOVECLIENT:
                            NodeData node = (NodeData) msg.getLoad();
                            System.out.println("Remove client message received from " + node.getIP());
                            directory.removeClient(node);
                            for (Map.Entry<Integer, NodeData> entry : successors.entrySet()) {
                                NodeData data = entry.getValue();
                                try (Socket tmpSocket = new Socket(data.getIP(), data.getPort())) {
                                    ObjectOutputStream outbound = new ObjectOutputStream(tmpSocket.getOutputStream());
                                    outbound.writeObject(msg);
                                }
                            }
                            break;
                        case Message.RECIEVE_DHT_DATA:
                            //transfer directory data to a different server upon quit
                            //not necessary; optional availability protection
                            //servers do not leave abruptly.
                            break;
                        default:  //unrecongnized message
                            System.out.println(msg);
                            break;
                    }

                    inSocket.close();
                } catch (Exception ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private static class ClientListener extends Thread {

        Message retMsg = null;
        ByteArrayOutputStream baos = null;
        ObjectOutputStream oos = null;
        byte[] Buf = null;
        int bufLen = 0;
        byte[] incData = new byte[10240];
        
        public ClientListener() {
        }

        @Override
        public void run() {

            while (bRunning) {
                try {
                    DatagramPacket packet = new DatagramPacket(incData, incData.length);
                    udpSocket.receive(packet);
                    
                    byte[] data = packet.getData();
                    
                    System.out.println("Receiving a message from client. " + data.length);
                    
                    ByteArrayInputStream in = new ByteArrayInputStream(data);
                    ObjectInputStream is = new ObjectInputStream(in);
                    
                    Message msg = (Message) is.readObject();
                    InetAddress sender = packet.getAddress();
                    int port = packet.getPort();
                    
                    switch (msg.getType()) {
                        case (Message.INIT):
                            System.out.println("Init message received from " + sender);
                            retMsg = new Message(Message.OK);
                            retMsg.setLoad(successors);
                            baos = new ByteArrayOutputStream();
                            oos = new ObjectOutputStream(baos);
                            oos.writeObject(retMsg);
                            oos.flush();
                            Buf = baos.toByteArray();

                            packet = new DatagramPacket(Buf, Buf.length, sender, port);
                            udpSocket.send(packet);                                
                            
                            break;
                        case (Message.INFORM):
                                
                            Content c = (Content) msg.getLoad();
                            if(c.getDHTid() == myData.getID()) {
                                directory.add(c);
                                System.out.println("Inform received from " + sender);
                                System.out.println("Adding content " + c.getName());
                                retMsg = new Message(Message.OK);
                                baos = new ByteArrayOutputStream();
                                oos = new ObjectOutputStream(baos);
                                oos.writeObject(retMsg);
                                oos.flush();
                                Buf= baos.toByteArray();

                                packet = new DatagramPacket(Buf, Buf.length, sender, port);
                                udpSocket.send(packet);              
                            }
                            else {
                                //Incorrect hash handler?
                                //practically imposssible.
                            }
                            
                            
                            break;
                        case (Message.QUERY):
                            System.out.println("Query message received from " + sender);
                            Content ctmp = (Content) msg.getLoad();
                            Content c1 = directory.get(ctmp.getName());
                            
                            if(c1 != null) {
                                System.out.println("Content found " + ctmp.getName());
                                retMsg = new Message(Message.OK);
                                retMsg.setLoad(c1);
                            }
                            else {
                                System.out.println("Content not found " + ctmp.getName());
                                retMsg = new Message(Message.NOTFOUND);
                            }
                            
                            baos = new ByteArrayOutputStream();
                            oos = new ObjectOutputStream(baos);
                            oos.writeObject(retMsg);
                            oos.flush();
                            Buf= baos.toByteArray();

                            packet = new DatagramPacket(Buf, Buf.length, sender, port);
                            udpSocket.send(packet);         
                            
                            break;
                            
                        case (Message.EXIT):
                            System.out.println("Exit message received from " + sender);
                            NodeData client = (NodeData) msg.getLoad();
                            directory.removeClient(client);
                            Message tcpMsg = new Message(Message.REMOVECLIENT);
                            tcpMsg.setLoad(client);
                            for (Map.Entry<Integer, NodeData> entry : successors.entrySet()) {
                                NodeData sd = entry.getValue();
                                try (Socket tmpSocket = new Socket(sd.getIP(), sd.getPort())) {
                                    ObjectOutputStream outbound = new ObjectOutputStream(tmpSocket.getOutputStream());
                                    outbound.writeObject(msg);
                                }
                            }
                            break;
                        case (Message.LIST):
                            //help function
                            //if time permits
                            break;
                         
                        default:
                            //handle unknown messages
                            break;

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
