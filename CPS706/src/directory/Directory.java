package directory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import util.*;

public class Directory {

    private Map<String, ArrayList<Content>> contentMap;
    private ArrayList<Content> tmpList;
    
    
    public Directory() {
        contentMap = Collections.synchronizedMap(new HashMap());
    }

    public void add(Content c) {
        boolean bExist = false;        
        if (contentMap.containsKey(c.getName())) {
            tmpList = contentMap.remove(c.getName());
            
            for (int i = 0; i< tmpList.size(); i++) {
                NodeData peer1 = tmpList.get(i).getPeer();
                NodeData peer2 = c.getPeer();
                if(peer1.getIP().hashCode() == peer2.getIP().hashCode() && peer1.getPort() == peer2.getPort() ) {
                    bExist = true;
                    break;
                }
            }
        }
        else {
            tmpList = new ArrayList();
        }
        if (!bExist) {
            tmpList.add(c);
            contentMap.put(c.getName(), tmpList);
        }
    }

    public Content get(String name) {
        if (contentMap.containsKey(name)) {
            return contentMap.get(name).get(0);
        }
        return null;
    }

    public void remove(String name) {
        contentMap.remove(name);
    }

    public void removeClient(NodeData peer2) {

        for (Map.Entry<String, ArrayList<Content>> entry : contentMap.entrySet()) {
            tmpList = entry.getValue();
            
            for (int i = 0; i< tmpList.size(); i++) {
                NodeData peer1 = tmpList.get(i).getPeer();
                if(peer1.getIP().hashCode() == peer2.getIP().hashCode() && peer1.getPort() == peer2.getPort() ) {
                    tmpList.remove(i);
                }
            }
            
            if(tmpList.isEmpty()) {
                contentMap.remove(entry.getKey());
            }
        }
    }
}
