package util;

import java.io.Serializable;
import java.net.InetAddress;

public class NodeData implements Serializable {

    static final long serialVersionUID = -50077493051991107L;
        
    private InetAddress iIPAddress;  
    private int iPort = 0;
    private int iID = 0;
    
    public NodeData() {
        
    } 
    
    public void setID(int id){
        iID=id;
    }
    
    public int getID() {
        return iID;
    }
    
    public void setPort(int port) {
        iPort=port;
    }
    
    public int getPort() {
        return iPort;
    }
    
    public InetAddress getIP () {
        return iIPAddress;
        
    }
    
    public void setIP(String ip) throws Exception {
        iIPAddress=InetAddress.getByName(ip);
    }

    public void setIP(InetAddress localHost) {
        iIPAddress = localHost;
    }
}
