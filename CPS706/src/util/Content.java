package util;

import java.io.File;
import java.io.Serializable;

public class Content implements Serializable {
    
    static final long serialVersionUID = -50077493051991107L;
    
    private int dhtId;
    
    private NodeData server;
    private NodeData peer;
    private File file;
    private String name;
    private long fileSize; 

    public Content(String name, int iServers) {
        this.name = name;
        uberHash(iServers);
    }

    public Content(File f, int iServers) {
        this.name = f.getName().substring(dhtId, f.getName().lastIndexOf('.'));   
        this.fileSize = f.length();
        this.file = f;
        uberHash(iServers);
    }

    
    private void uberHash(int iServers) {
        int tempVal = 0;
	
        for(int i = 0; i < name.length(); i++) {
            tempVal += (int) this.name.charAt(i);
	}

        this.dhtId = tempVal % iServers + 1;
    }
	
    public int getDHTid() { return this.dhtId; }
	
    public void setDHTid(int dhtid) { this.dhtId = dhtid; }
	
    public String getName() { return this.name; }
	
    public File getFile() { return this.file; }
    
    public void setServerData(NodeData sd) {
        this.server=sd;
    }
    
    public void setPeer (NodeData peer) {
        this.peer = peer;
    }
    
    public NodeData getServerData() {
        return server;
    }
    
    public void setFile(File f) { this.file = f; }
    
    public NodeData getPeer() {
        return peer;
    }
    
}
