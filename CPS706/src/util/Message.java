package util;

import java.io.Serializable;

public class Message implements Serializable {

    static final long serialVersionUID = -50077493051991107L;
    
    public static final int ACK = 100;
    
    public static final int QUIT = 1;
    public static final int REMOVECLIENT = 2;
    public static final int RECIEVE_DHT_DATA = 3;
        
    public static final int INIT = 10;
    public static final int INFORM = 11;
    public static final int QUERY = 12;
    public static final int EXIT = 13;
    public static final int LIST = 14;
    public static final int GET = 15;
    public static final int OK = 200;
    public static final int ERROR = 400;
    public static final int NOTFOUND = 404;
    public static final int NOTSUPPORTED = 505;
    
    private int type;
    private Object load = null;

    public Message(int messageType) {
        type = messageType;
    }

    public void setLoad(Object o) {
        load = o;
    }
    
    public Object getLoad() {
        return load;
    } 
    
    public int getType() {
        return type;
    }
    
}
