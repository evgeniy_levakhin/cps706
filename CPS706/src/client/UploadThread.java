package client;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;
import util.*;

public class UploadThread extends Thread {
    
    Content content;
    Socket socket;
    
    public UploadThread(Content content, Socket socket) {
        this.content = content;
        this.socket = socket;
        
    }

    public void run() {
        
        System.out.println("Uploading "+ content.getName());
        
        File f = content.getFile();
        BufferedOutputStream output = null;
        FileInputStream in = null;
        byte[] buf ;
        try {
            buf = new byte[(int) f.length()];
            output = new BufferedOutputStream(socket.getOutputStream());
            in = new FileInputStream(f);
            BufferedInputStream bis = new BufferedInputStream(in);
            
            bis.read(buf, 0, buf.length);
            output.write(buf, 0, buf.length);
            output.flush();
            output.close();
            socket.close();
           System.out.println("Uploading complete");
        } catch (IOException e1) {
            e1.printStackTrace();
        } 
    }
}
