package client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import util.*;

public class Client {

    public static NodeData self = new NodeData();
    public static Set<Object> contentSet = Collections.synchronizedSet(new HashSet<>());
    public static HashMap<String, InetAddress> peers = new HashMap<String, InetAddress>();
    public static Map<Integer, NodeData> servers = new HashMap<>();
    
    private static boolean bRunning = true;

    public static void main(String[] args) {
        
        //String[] args = {"40156","127.0.0.1","40150"};
        
        if (args.length != 3) {
            System.err.println("Incorrect starting parameters");
            System.err.println("java AppName PortNum Server1IP Server1Port");
            System.exit(-1);
        }
        
        P2PServer p2pserver = null;
        try {
            self.setIP(InetAddress.getLocalHost());
            self.setPort(Integer.parseInt(args[0]));
            
            NodeData server1 = new NodeData();
            server1.setID(1);

            server1.setIP(args[1]);
            server1.setPort(Integer.parseInt(args[2]));
            servers.put(1, server1);

            p2pserver = new P2PServer(self.getPort());
            p2pserver.start();

            System.out.println("Client is starting up");
            System.out.println("Sending init command");
            init();

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            while (bRunning) {
                System.out.println("Menu (enter menu number)");
                System.out.println("1. Init");
                System.out.println("2. Inform");
                System.out.println("3. Query");
                System.out.println("3. View Directory of the Server. (Optional. Not implemented)");
                System.out.println("4. Exit");
                String line = in.readLine();
                if (line.length() >= 1) {
                    switch (line.charAt(0)) {
                        case ('1'):
                            init();
                            break;
                        case ('2'):
                            System.out.println("Enter full path of the content you wish to add");
                            String name = in.readLine();
                            File f = new File(name);
                            if (f.exists()) {
                                if (f.getName().endsWith(".jpg") || f.getName().endsWith(".jpeg")) {
                                    Content c = new Content(f, servers.size());
                                    c.setPeer(self);
                                    inform(c);
                                } else {
                                    System.out.println("Not a jpeg file");
                                }
                            } else {
                                System.out.println("File not found");
                            }
                            break;
                        case ('3'):
                            System.out.println("Enter the name of the content your looking for...");
                            String tmp = in.readLine();
                            Content ctmp = new Content(tmp, servers.size());
                            System.out.println("Quering servers for " + tmp);
                            query(ctmp);
                            break;
                        case ('4'):
                            exit();
                            bRunning = false;
                            break;
                    }
                }
            }

            System.out.println("Client: Forced exit.");
            System.exit(0);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void exit() throws Exception {
        System.out.println("Exit");
        Message m = new Message(Message.EXIT);

        for (int i = 1; i <= servers.size(); i++) {
            NodeData data = servers.get(i);

            if (data != null) {
                Message msg = new Message(Message.EXIT);
                msg.setLoad(self);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(msg);
                oos.flush();
                byte[] Buf = baos.toByteArray();

                DatagramSocket udpSocket = new DatagramSocket(self.getPort());
                DatagramPacket packet = new DatagramPacket(Buf, Buf.length, data.getIP(), data.getPort());
                udpSocket.send(packet);
                udpSocket.close();
            }
        }
    }

    public static void init() {

        boolean bReady = false;
        DatagramSocket udpSocket = null;
        while (!bReady) {
            try {
                int iTotalServers = servers.size();

                for (int i = 1; i <= servers.size(); i++) {

                    NodeData data = servers.get(i);
                    if (data != null) {
                        Message msg = new Message(Message.INIT);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ObjectOutputStream oos = new ObjectOutputStream(baos);
                        oos.writeObject(msg);
                        oos.flush();
                        byte[] Buf = baos.toByteArray();
                        udpSocket = new DatagramSocket(self.getPort());

                        DatagramPacket packet = new DatagramPacket(Buf, Buf.length, data.getIP(), data.getPort());
                        udpSocket.send(packet);

                        byte[] incoming = new byte[10240];
                        packet = new DatagramPacket(incoming, incoming.length);

                        udpSocket.setSoTimeout(10000);
                        udpSocket.receive(packet);

                        byte[] incdata = packet.getData();

                        ByteArrayInputStream in = new ByteArrayInputStream(incdata);
                        ObjectInputStream is = new ObjectInputStream(in);

                        Message incmsg = (Message) is.readObject();

                        Map<Integer, NodeData> mapServers = (Map<Integer, NodeData>) incmsg.getLoad();

                        for (Map.Entry<Integer, NodeData> entry : mapServers.entrySet()) {
                            NodeData sd = entry.getValue();

                            if (!servers.containsKey(entry.getKey())) {
                                servers.put(entry.getKey(), sd);
                            }
                        }

                        udpSocket.close();
                    }
                }

                if (iTotalServers == servers.size()) {
                    bReady = true;
                }
            } catch (Exception ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void inform(Content c) throws Exception {
        NodeData data = servers.get(c.getDHTid());
        boolean bReady = false;
        if (data != null) {
            while (!bReady) {

                Message msg = new Message(Message.INFORM);
                msg.setLoad(c);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(msg);
                oos.flush();
                byte[] Buf = baos.toByteArray();

                DatagramSocket udpSocket = new DatagramSocket(self.getPort());
                DatagramPacket packet = new DatagramPacket(Buf, Buf.length, data.getIP(), data.getPort());
                udpSocket.send(packet);

                byte[] incoming = new byte[10240];
                packet = new DatagramPacket(incoming, incoming.length);

                udpSocket.setSoTimeout(10000);
                udpSocket.receive(packet);

                byte[] incdata = packet.getData();

                ByteArrayInputStream in = new ByteArrayInputStream(incdata);
                ObjectInputStream is = new ObjectInputStream(in);

                Message incmsg = (Message) is.readObject();

                if (incmsg.getType() == Message.OK) {
                    System.out.println("The server has been informed about the file " + c.getName());
                    c.setServerData(data);
                    contentSet.add(c);
                    bReady = true;
                }
                udpSocket.close();
            }
        } else {
            System.out.println("Unable to inform server " + c.getDHTid());
            System.out.println("Server not found");
        }
    }

    public static void query(Content c) throws Exception {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        NodeData data = servers.get(c.getDHTid());
        boolean bReady = false;
        if (data != null) {
            while (!bReady) {

                Message msg = new Message(Message.QUERY);
                msg.setLoad(c);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(msg);
                oos.flush();
                byte[] Buf = baos.toByteArray();

                DatagramSocket udpSocket = new DatagramSocket(self.getPort());
                DatagramPacket packet = new DatagramPacket(Buf, Buf.length, data.getIP(), data.getPort());
                udpSocket.send(packet);

                byte[] incoming = new byte[10240];
                packet = new DatagramPacket(incoming, incoming.length);

                udpSocket.setSoTimeout(10000);
                udpSocket.receive(packet);

                byte[] incdata = packet.getData();

                ByteArrayInputStream in = new ByteArrayInputStream(incdata);
                ObjectInputStream is = new ObjectInputStream(in);

                Message incmsg = (Message) is.readObject();

                if (incmsg.getType() == Message.OK) {
                    System.out.println("The content has been located ");
                    System.out.println("Would you like to download it? y/n ");
                    String answer = console.readLine();
                    boolean difficultUser = true;
                    while (difficultUser) {
                        switch (answer) {
                            case "y":
                                difficultUser = false;
                                c = (Content) incmsg.getLoad();
                                (new DownloadThread(c,self)).start();
                                break;
                            case "n":
                                difficultUser = false;
                                System.out.println("Ok... returning to main menu ");
                                break;
                            default:
                                difficultUser = true;
                                System.out.println("Enter y or n.. ");
                                break;
                        }
                    }

                    bReady = true;
                }
                else {
                    System.out.println("Content cannot be located... ");
                    System.out.println("Server returned: " + incmsg.getType());
                    bReady = true;
                }
                udpSocket.close();
            }
        } else {
            System.out.println("Unable to inform server " + c.getDHTid());
            System.out.println("Server not found");
        }
    }
}
