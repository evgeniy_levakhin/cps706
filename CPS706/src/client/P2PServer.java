package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.*;

public class P2PServer extends Thread {

    private ServerSocket ss;

    public P2PServer(int Port) throws IOException {
        ss = new ServerSocket(Port);
    }

    public void run() {
        while (true) {
            try {
                Socket s = ss.accept();
                ObjectInputStream incoming = new ObjectInputStream(s.getInputStream());
                Message msg = (Message) incoming.readObject();

                switch(msg.getType()) {
                    case Message.GET:
                        Content c = (Content) msg.getLoad();
                        (new UploadThread(c,s)).start();
                        break;
                    default: 
                        Message ret = new Message(Message.ERROR);
                        ObjectOutputStream outbound = new ObjectOutputStream(s.getOutputStream());
                        outbound.writeObject(msg);
                        s.close();
                        break;
                }
                
            } catch (Exception ex) {
                 Logger.getLogger(P2PServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
