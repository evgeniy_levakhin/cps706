package client;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import util.*;

public class DownloadThread extends Thread {
    Content content;
    NodeData self;

   public DownloadThread(Content c, NodeData self) {
        this.content=c;
        this.self = self;
    }

    @Override
    public void run() {
        try {
            byte[] aByte = new byte[1];
            int bytesRead;
            
            NodeData node = content.getPeer();
            
            Message msg = new Message(Message.GET);
            msg.setLoad(content);

            System.out.println("Starting Download " + content.getName());
            Socket socket = new Socket(node.getIP(), node.getPort());
            ObjectOutputStream outbound = new ObjectOutputStream(socket.getOutputStream());
            outbound.writeObject(msg);
            
            InputStream is = socket.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            
            FileOutputStream fos = new FileOutputStream( content.getFile().getName());
            BufferedOutputStream bos = new BufferedOutputStream(fos);;
            bytesRead = is.read(aByte, 0, aByte.length);
            
            do {
                        baos.write(aByte);
                        bytesRead = is.read(aByte);
                } while (bytesRead != -1);
            
            bos.write(baos.toByteArray());
            bos.flush();
            bos.close();
            socket.close();
            
            System.out.println("Download Complete");
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
}
